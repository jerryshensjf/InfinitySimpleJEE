package org.javaforever.infinity.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.core.JQueryPositions;
import org.javaforever.infinity.core.Writeable;
import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.DragonHideStatement;
import org.javaforever.infinity.domain.JavascriptMethod;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.domain.Signature;
import org.javaforever.infinity.domain.Statement;
import org.javaforever.infinity.domain.StatementList;
import org.javaforever.infinity.domain.Type;
import org.javaforever.infinity.domain.Var;
import org.javaforever.infinity.generator.NamedStatementGenerator;
import org.javaforever.infinity.generator.NamedStatementListGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.SqlReflector;
import org.javaforever.infinity.utils.StringUtil;
import org.javaforever.infinity.utils.WriteableUtil;

public class Revoke extends TwoDomainVerb implements JQueryPositions{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("revoke"+this.slave.getCapFirstDomainName()+"From"+this.master.getCapFirstDomainName());	
		method.setThrowException(true);
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		method.addSignature(new Signature(3, StringUtil.lowerFirst(this.slave.getStandardName())+"Id", this.slave.getDomainId().getClassType()));
		method.setReturnType(new Type("Integer"));
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,2, "String "+InterVarUtil.DB.query.getVarName()+" = \""+SqlReflector.generateDeleteLinkTwoSql(master,slave) +"\";"));
		list.add(NamedStatementGenerator.getPrepareStatementWithReturnGeneratedKeys(200L,2, InterVarUtil.DB.ps, InterVarUtil.DB.query, InterVarUtil.DB.connection));
		list.add(new Statement(300L,2,InterVarUtil.DB.ps.getVarName()+"."+this.master.getDomainId().getClassType().getTypeSetterName()+"(1,"+this.master.getLowerFirstDomainName()+"Id);"));
		list.add(new Statement(400L,2,InterVarUtil.DB.ps.getVarName()+"."+this.slave.getDomainId().getClassType().getTypeSetterName()+"(2,"+this.slave.getLowerFirstDomainName()+"Id);"));
		list.add(new Statement(500L,2,"int result = "+InterVarUtil.DB.ps.getVarName()+".executeUpdate();"));
		list.add(new Statement(600L,2,"return result;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName("revoke"+this.slave.getCapFirstDomainName()+"From"+this.master.getCapFirstDomainName());
		method.setReturnType(new Type("Integer"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		
		method.addSignature(new Signature(1,InterVarUtil.DB.connection.getVarName(),InterVarUtil.DB.connection.getVarType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		method.addSignature(new Signature(3, StringUtil.lowerFirst(this.slave.getStandardName())+"Id", this.slave.getDomainId().getClassType()));
		return method;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		return method;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(getVerbName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.master.getPackageToken()+".dao."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+".service."+this.slave.getStandardName()+"Service");
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.master.getStandardName())+"Id", this.master.getDomainId().getClassType()));
		method.addSignature(new Signature(2, StringUtil.lowerFirst(this.slave.getStandardName())+"Ids", new Type("String")));
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementGenerator.getDBConfInitDBAutoRelease(500L, 2, InterVarUtil.DB.connection, InterVarUtil.DB.dbconf));
		list.add(new Statement(1000L,3,"String [] "+this.slave.getLowerFirstDomainName()+"IdsArr = "+this.slave.getLowerFirstDomainName()+"Ids.split(\",\");"));
		list.add(new Statement(2000L,3,"for (int i = 0; i < "+this.slave.getLowerFirstDomainName()+"IdsArr.length; i++ ){"));
		list.add(new DragonHideStatement(3000L,4,"String "+this.slave.getLowerFirstDomainName()+"Id = "+this.slave.getLowerFirstDomainName()+"IdsArr[i];",this.slave.getDomainId().getClassType().toString().equals("String")));
		list.add(new DragonHideStatement(3000L,4,"Long "+this.slave.getLowerFirstDomainName()+"Id = Long.parseLong("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Long")));
		list.add(new DragonHideStatement(3000L,4,"Integer "+this.slave.getLowerFirstDomainName()+"Id = Integer.parseInt("+this.slave.getLowerFirstDomainName()+"IdsArr[i]);",this.slave.getDomainId().getClassType().toString().equals("Integer")));
		list.add(new Statement(4000L,4,"Integer success = dao."+generateDaoMethodDefinition().generateStandardCallString()+";"));
		list.add(new Statement(5000L,4,"if (success < 0) return false;"));
		list.add(new Statement(6000L,3,"}"));
		list.add(new Statement(7000L,3,"return true;"));
		list.add(new Statement(7500L,2,"}"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("processRequest");
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		List<String> list = new ArrayList<String>();
		list.add("ServletException");
		list.add("IOException");
		method.setIsprotected(true);
		method.setOtherExceptions(list);
		method.addSignature(new Signature(1,"request",new Type("HttpServletRequest","javax.servlet.http")));
		method.addSignature(new Signature(2,"response",new Type("HttpServletResponse","javax.servlet.http")));
		method.addAdditionalImport("java.io.IOException");
		method.addAdditionalImport("java.io.PrintWriter");
		method.addAdditionalImport("javax.servlet.ServletException");
		method.addAdditionalImport("javax.servlet.http.HttpServlet");
		method.addAdditionalImport("javax.servlet.http.HttpServletRequest");
		method.addAdditionalImport("javax.servlet.http.HttpServletResponse");
		method.addAdditionalImport("java.util.Map");
		method.addAdditionalImport("java.util.TreeMap");
		method.addAdditionalImport("net.sf.json.JSONObject");
		method.addAdditionalImport(this.master.getPackageToken()+".utils.StringUtil");
		method.addAdditionalImport(this.master.getPackageToken()+".domain."+this.master.getStandardName());
		method.addAdditionalImport(this.slave.getPackageToken()+".domain."+this.slave.getStandardName());
		method.addAdditionalImport(this.master.getPackageToken()+".service."+this.master.getStandardName()+"Service");
		method.addAdditionalImport(this.master.getPackageToken()+".serviceimpl."+this.master.getStandardName()+"ServiceImpl");
		
		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.master.getStandardName()+"Service",this.master.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedStatementGenerator.getFacadeSetContentType(1000L, 2, InterVarUtil.Servlet.response, InterVarUtil.SimpleJEE.UTF8.getVarName()));
		wlist.add(NamedStatementGenerator.getControllerPrintWriterOut(2000L, 2, InterVarUtil.Servlet.response, InterVarUtil.Servlet.out));
		wlist.add(NamedStatementGenerator.getJsonResultMap(3000L, 2, resultMap));
		wlist.add(new Statement(4000L,2, service.getVarType() +" " +service.getVarName() +" = new " +service.getVarType().getClassType()+"Impl();"));
		wlist.add(NamedStatementGenerator.getTryHead(5000L, 2));
		wlist.add(new Statement(6000L,3,this.master.getDomainId().getFieldType()+ " " + this.master.getLowerFirstDomainName()+"Id = "+Type.getInitTypeWithVal(this.master.getDomainId().getClassType())+";"));
		wlist.add(new Statement(7000L,3,"String " + this.slave.getLowerFirstDomainName()+"Ids = \"\";"));
		wlist.add(new DragonHideStatement(8000L,3,"if (!StringUtil.isBlank(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"))) "+this.master.getLowerFirstDomainName()+"Id = Long.parseLong(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"));",this.master.getDomainId().getFieldType().equalsIgnoreCase("long")));
		wlist.add(new DragonHideStatement(8000L,3,"if (!StringUtil.isBlank(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"))) "+this.master.getLowerFirstDomainName()+"Id = Integer.parseInt(request.getParameter(\""+this.master.getLowerFirstDomainName()+"Id\"));",this.master.getDomainId().getFieldType().equals("int")||this.master.getDomainId().getFieldType().equals("Integer")));
		wlist.add(new Statement(9000L,3,"if (!StringUtil.isBlank(request.getParameter(\""+this.slave.getLowerFirstDomainName()+"Ids\"))) "+this.slave.getLowerFirstDomainName()+"Ids = request.getParameter(\""+this.slave.getLowerFirstDomainName()+"Ids\");"));
		wlist.add(new Statement(10000L,3,"boolean success = "+service.getVarName()+"."+this.generateServiceMethodDefinition().getStandardCallString()+";"));
		wlist.add(new Statement(11000L,3, resultMap.getVarName()+".put(\"success\",success);"));
		wlist.add(new Statement(12000L,3, resultMap.getVarName()+".put(\"data\",null);"));
		wlist.add(NamedStatementGenerator.getEncodeMapToJsonResultMap(13000L, 3, resultMap,InterVarUtil.Servlet.out));
		wlist.add(NamedStatementListGenerator.generateCatchExceptionPrintStackPrintJsonMapFinallyCloseOutFooter(14000L, 2, InterVarUtil.Servlet.response, resultMap, InterVarUtil.Servlet.out));
		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}
	
	public Revoke(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.setVerbName("Revoke"+this.slave.getCapFirstPlural()+"From"+this.master.getCapFirstDomainName());
		this.setLabel("撤销");
	}

	@Override
	public JavascriptMethod generateJQueryActionMethod() throws Exception {
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "var "+this.slave.getLowerFirstDomainName()+"Ids = $(\"#my"+this.slave.getCapFirstPlural()+"\").val() || [] ;"));
		sl.add(new Statement(3000,1, "if (isBlank("+this.slave.getLowerFirstDomainName()+"Ids)) {"));
		sl.add(new Statement(4000,2, "$.messager.alert(\"操作提示\", \"请选择现有"+this.slave.getCapFirstDomainName()+"！\");"));
		sl.add(new Statement(5000,2, "return;"));
		sl.add(new Statement(6000,1, "}"));
		sl.add(new Statement(10000,1, "var "+this.master.getLowerFirstDomainName()+"Id = $(\"#my"+this.master.getCapFirstDomainName()+"\").val()[0];"));
		sl.add(new Statement(11000,1, "$.ajax({"));
		sl.add(new Statement(12000,2, "type: \"post\","));
		sl.add(new Statement(13000,2, "url: \"../facade/"+StringUtil.lowerFirst(this.getVerbName())+"Facade\","));
		sl.add(new Statement(14000,2, "data: {"));
		sl.add(new Statement(15000,3, this.slave.getLowerFirstDomainName()+"Ids:"+this.slave.getLowerFirstDomainName()+"Ids.join(\",\"),"));
		sl.add(new Statement(16000,3, this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id,"));
		sl.add(new Statement(17000,2, "},"));
		sl.add(new Statement(18000,2, "dataType: 'json',"));
		sl.add(new Statement(19000,2, "success: function(data, textStatus) {"));
		sl.add(new Statement(20000,3, "if (data.success) {"));
		sl.add(new Statement(20100,4, "loadMy"+this.master.getCapFirstPlural()+"();"));
		sl.add(new Statement(20200,3, "}"));
		sl.add(new Statement(21000,2, "},"));
		sl.add(new Statement(22000,2, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(23000,2, "},"));
		sl.add(new Statement(24000,2, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(25000,3, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(26000,3, "alert(errorThrown.toString());"));
		sl.add(new Statement(27000,2, "}"));
		sl.add(new Statement(28000,1, "});"));
		
		method.setMethodStatementList(sl);
		return method;
	}

	@Override
	public String generateJQueryActionString() throws Exception {
		return generateJQueryActionMethod().generateMethodString();
	}

	@Override
	public String generateJQueryActionStringWithSerial() throws Exception {
		return generateJQueryActionMethod().generateMethodStringWithSerial();
	}
}
