package org.javaforever.infinity.complexverb;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Method;

public abstract class TwoDomainVerb implements Comparable<TwoDomainVerb>{
	protected Domain master;
	protected Domain slave;
	protected String label;
	protected String verbName;
	
	public abstract Method generateDaoImplMethod() throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract Method generateControllerMethod() throws Exception;
	
	public Domain getMaster() {
		return master;
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getText(){
		if (this.label!= null && !this.label.equals("")) return this.label;
		else return this.verbName;
	}
	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	
	@Override
	public int compareTo(TwoDomainVerb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
}
