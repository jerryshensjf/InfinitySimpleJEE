package org.javaforever.infinity.core;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.infinity.domain.Domain;
import org.javaforever.infinity.domain.Method;
import org.javaforever.infinity.limitedverb.DaoOnlyVerb;
import org.javaforever.infinity.limitedverb.NoControllerVerb;

public abstract class Verb implements Comparable<Verb>{
	protected String verbName;
	protected Method method;	
	protected String verbComment;
	protected String verbContent;
	protected Domain domain;
	protected List<String> additionalImports = new ArrayList<String>();
	protected List<NoControllerVerb> noControllerVerbs = new ArrayList<NoControllerVerb>();
	protected List<DaoOnlyVerb> daoOnlyVerbs = new ArrayList<DaoOnlyVerb>();
	public List<NoControllerVerb> getNoControllerVerbs() {
		return noControllerVerbs;
	}
	public void setNoControllerVerbs(List<NoControllerVerb> noControllerVerbs) {
		this.noControllerVerbs = noControllerVerbs;
	}
	public List<DaoOnlyVerb> getDaoOnlyVerbs() {
		return daoOnlyVerbs;
	}
	public void setDaoOnlyVerbs(List<DaoOnlyVerb> daoOnlyVerbs) {
		this.daoOnlyVerbs = daoOnlyVerbs;
	}

	public String getVerbName() {
		return verbName;
	}
	public void setVerbName(String verbName) {
		this.verbName = verbName;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}

	public String getVerbComment() {
		return verbComment;
	}
	public void setVerbComment(String verbComment) {
		this.verbComment = verbComment;
	}
	public String getVerbContent() {
		return verbContent;
	}
	public void setVerbContent(String verbContent) {
		this.verbContent = verbContent;
	}
	
	public abstract Method generateDaoImplMethod()  throws Exception;
	public abstract Method generateDaoMethodDefinition() throws Exception;
	public abstract Method generateServiceMethodDefinition() throws Exception;
	public abstract Method generateServiceImplMethod() throws Exception;
	public abstract Method generateControllerMethod() throws Exception;
	
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public List<String> getAdditionalImports() {
		return additionalImports;
	}
	public void setAdditionalImports(List<String> additionalImports) {
		this.additionalImports = additionalImports;
	}
	
	public Verb(Domain domain){
		super();
		this.domain = domain;
	}
	
	public Verb(){
		super();
	}
	
	@Override
	public int compareTo(Verb o) {
		return this.getVerbName().compareTo(o.getVerbName());
	}
}
