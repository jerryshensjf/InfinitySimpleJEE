package org.javaforever.infinity.domain;

import java.util.Iterator;
import java.util.Set;

import org.javaforever.infinity.complexverb.TwoDomainVerb;
import org.javaforever.infinity.exception.ValidateException;
import org.javaforever.infinity.generator.NamedUtilMethodGenerator;
import org.javaforever.infinity.utils.InterVarUtil;
import org.javaforever.infinity.utils.StringUtil;

public class TwoDomainFacade extends Class implements Comparable<TwoDomainFacade>{
	private static final long serialVersionUID = 229666513964270905L;
	protected TwoDomainVerb tdverb;
	protected Domain master;
	protected Domain slave;
	
	public String generateFacadeString()  throws ValidateException{
		this.tdverb.setMaster(master);
		this.tdverb.setSlave(slave);
		//Method verbMethod = this.verb.generateFacadeMethod();
		//this.addMethod(verbMethod);
		StringBuilder sb = new StringBuilder();
		if (this.packageToken !=null && !"".equalsIgnoreCase(this.packageToken)) sb.append("package ").append(this.packageToken).append(".").append("facade").append(";\n\n");
		Set<String> imports = this.generateImportStrings();
		for (String s: imports){
			sb.append("import ").append(s).append(";\n");
		}
		sb.append("\n");
	
		sb.append("public class ").append(this.getStandardName()).append(" extends HttpServlet{\n");		
		
		//generate fields notions
		Iterator it = this.getFields().iterator();
		  while (it.hasNext()) {	
		        Field field = (Field) it.next();
		        String fieldName = field.getFieldName();
		        String fieldType = field.getFieldType();
		        sb.append("\tprotected ").append(fieldType).append(" ").append(fieldName).append(";\n");
	        }
		  sb.append("\n");
		  //generate getter setter notions
		  it = this.getFields().iterator();
		  while (it.hasNext()) {	
		        Field field = (Field) it.next();
		        String fieldName = field.getFieldName();
		        String fieldType = field.getFieldType();
		        // generate setters
		        sb.append("\tpublic void set").append(StringUtil.capFirst(fieldName)).append("(").append(fieldType).append(" ").append(StringUtil.lowerFirst(fieldName)).append("){\n");
		        sb.append("\t\tthis.").append(StringUtil.lowerFirst(fieldName)).append(" = ").append(StringUtil.lowerFirst(fieldName)).append(";\n");
		        sb.append("\t}\n\n");
		        // generate getters
		        sb.append("\tpublic ").append(fieldType).append(" get").append(StringUtil.capFirst(fieldName)).append("(){;\n");
		        sb.append("\t\treturn this.").append(StringUtil.lowerFirst(fieldName)).append(";\n");
		        sb.append("\t}\n\n");
	        }
		  
		  Iterator it2 = this.getMethods().iterator();
		  while (it2.hasNext()){
			  sb.append(((Method)it2.next()).generateMethodString()).append("\n");
		  }
		  sb.append("}\n");
		return sb.toString();
	}

	public TwoDomainFacade(TwoDomainVerb  twoverb, Domain master, Domain slave) throws Exception{
		super();
		twoverb.setMaster(master);
		twoverb.setSlave(slave);
		this.master = master;
		this.slave = slave;
		this.standardName = twoverb.getVerbName() + "Facade";
		this.tdverb = twoverb;
		this.packageToken = master.getPackageToken();
		
		ValidateInfo info = this.validate();
		if (info.success){
			Method processRequest = this.tdverb.generateControllerMethod();
			Method doGet = NamedUtilMethodGenerator.generateServletDoGetCallProcessRequestMethod(2, InterVarUtil.Servlet.request, InterVarUtil.Servlet.response);
			Method doPost =  NamedUtilMethodGenerator.generateServletDoPostCallProcessRequestMethod(2, InterVarUtil.Servlet.request, InterVarUtil.Servlet.response);
			Method servletInfo = NamedUtilMethodGenerator.generateGetServletInfoMethod(2, "Powered by Mind Rules.");
			this.addMethod(processRequest);
			this.addMethod(servletInfo);
			this.addMethod(doGet);
			this.addMethod(doPost);
		}else {
			ValidateException e = new ValidateException(info);
			throw e;
		}
	}
	
	public TwoDomainFacade(){super();}

	@Override
	public int compareTo(TwoDomainFacade o) {
		String myName = this.getStandardName();
		String otherName = o.getStandardName();
		return myName.compareTo(otherName);
	}
}
