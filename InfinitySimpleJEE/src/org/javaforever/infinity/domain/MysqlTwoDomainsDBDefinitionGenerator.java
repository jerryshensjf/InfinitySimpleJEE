package org.javaforever.infinity.domain;

import org.javaforever.infinity.utils.SqlReflector;

public class MysqlTwoDomainsDBDefinitionGenerator{
	protected Domain master;
	protected Domain slave;
	
	public MysqlTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql(String dbtype) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append(SqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		return sb.toString();
	}
	
	public String generateDropLinkTableSql() throws Exception{
		StringBuilder sb = new StringBuilder();

		sb.append(SqlReflector.generateDropLinkTableSql(this.master,this.slave)).append("\n");
		return sb.toString();
	}
}
